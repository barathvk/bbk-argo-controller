FROM node:alpine
COPY . /app
WORKDIR /app
RUN yarn --non-interactive --production
RUN yarn build
ENTRYPOINT [ "yarn", "start" ]