import JSONStream from 'json-stream'
import request from 'request'
import axios from 'axios'
const url = 'http://localhost:8001/apis/extensions/v1beta1/ingresses?watch=true'
const client = axios.create({
  baseURL: 'http://localhost:8001'
})
const init = async () => {
  await client.get('/api/v1/namespaces/cloudflared').catch(async err => {
    if (err.response.data.code === 404) {
      await client.post('/api/v1/namespaces', {
        apiVersion: 'v1',
        kind: 'Namespace',
        metadata: {
          name: 'cloudflared'
        }
      })
    }
  })
}
const manifest = (ingress, count, hostname, backend) => {
  return {
    apiVersion: 'v1',
    kind: 'Pod',
    metadata: {
      name: `${ingress.metadata.namespace}-${ingress.metadata.name}-${count}`,
      namespace: 'cloudflared',
      labels: {
        app: `${ingress.metadata.namespace}-${ingress.metadata.name}`
      }
    },
    spec: {
      volumes: [
        {
          name: 'cloudflared-cert',
          secret: {
            secretName: 'cloudflared-cert'
          }
        }
      ],
      containers: [
        {
          name: 'cloudflared',
          image: 'lennonsaves/cloudflared',
          args: [
            '--hostname',
            hostname,
            backend
          ],
          volumeMounts: [
            {
              name: 'cloudflared-cert',
              mountPath: '/root/.cloudflared'
            }
          ]
        }
      ]
    }
  }
}
const deleted = async ingress => {
  const purl = `/api/v1/namespaces/cloudflared/pods?labelSelector=app%3D${ingress.metadata.namespace}-${ingress.metadata.name}`
  const pods = await client.get(purl).then(d => d.data.items.map(p => p.metadata.name)).catch(err => {
    console.error(`[${err.response.data.code}] ${err.response.data.message}`)
  })
  pods.map(p => {
    client.delete(`/api/v1/namespaces/cloudflared/pods/${p}?gracePeriodSeconds=0`).catch(err => {
      console.error(`[${err.response.data.code}] ${err.response.data.message}`)
    })
  })
}
const added = async ingress => {
  const rules = ingress.spec.rules
  let count = 0
  rules.map(async rule => {
    const hostname = rule.host
    const backend = `http://${rule.http.paths[0].backend.serviceName}.${ingress.metadata.namespace}:${rule.http.paths[0].backend.servicePort}`
    console.log(`[ADD RULE] ${hostname} -> ${backend}`)
    await client.post('/api/v1/namespaces/cloudflared/pods', manifest(ingress, count, hostname, backend)).catch(err => {
      console.error(`[${err.response.data.code}] ${err.response.data.message}`)
    }).then(() => {
      console.log(`[ADD POD] ${ingress.metadata.namespace}-${ingress.metadata.name}-${count}`)
    })
    count++
  })
}
const modified = async ingress => {
  await deleted(ingress)
  await added(ingress)
}
const watch = () => {
  const stream = JSONStream()
  stream.on('data', item => {
    const ingclass = item.object.metadata.annotations['kubernetes.io/ingress.class']
    if (ingclass === 'argo-tunnel') {
      console.log(`[${item.type}] ${item.object.metadata.namespace}/${item.object.metadata.name}`)
      switch (item.type) {
        case 'ADDED':
          added(item.object)
          break
        case 'MODIFIED':
          modified(item.object)
          break
        case 'DELETED':
          deleted(item.object)
          break
        default:
          break
      }
    } else {
      console.log(`[IGNORED] ${item.object.metadata.namespace}/${item.object.metadata.name} - Ingress class ${ingclass}`)
    }
  }).on('end', watch)
  request(url).pipe(stream)
}
init().then(watch)
